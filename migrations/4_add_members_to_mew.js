var Distributor = artifacts.require('./Distributor.sol')
let getAccounts = require('../js/getAccounts.js')

module.exports = function (deployer) {
  deployer.then(async () => {
    let accounts = await getAccounts(deployer.network, web3, true)
    console.log('adding', accounts.length, 'members to mew...')
    let DistributorInstance = await Distributor.deployed()
    for (let i = 0; i < accounts.length; i++) {
      let initialWeight = Math.floor((Math.random() * 100))
      await (accounts[i], initialWeight)
      await DistributorInstance.addInitialMember(accounts[i], initialWeight)
      if (deployer.network !== 'development') console.log('added', accounts[i])
    }
    let remainingMEW = (await DistributorInstance.nInitialMembers()).toNumber() - accounts.length
    console.log(remainingMEW, 'more addresses can be added in the future')
  })
}
