var Math = artifacts.require('Math')
var MathWrapper = artifacts.require('./mocks/MathWrapper')
var Concept = artifacts.require('./Concept.sol')
var Assessment = artifacts.require('./Assessment.sol')
var NewAssessment = artifacts.require('./NewAssessment.sol')
var ConceptRegistry = artifacts.require('./ConceptRegistry.sol')
var ProxyFactory = artifacts.require('./ProxyFactory.sol')
var FathomToken = artifacts.require('./FathomToken.sol')
var Distributor = artifacts.require('./Distributor.sol')
var Minter = artifacts.require('./Minter.sol')

let getAccounts = require('../js/getAccounts.js')

module.exports = async function (deployer) {
  await deployer.deploy(Math)
  await Math.deployed()
  await deployer.link(Math, [MathWrapper, Assessment, NewAssessment, Concept, ConceptRegistry, ProxyFactory])
  await deployer.deploy(MathWrapper)

  // set up the proxy factory
  await deployer.deploy(Assessment)
  await deployer.deploy(NewAssessment) // used for network upgrade
  await deployer.deploy(Concept)
  await deployer.deploy(ProxyFactory, Concept.address, Assessment.address)

  await deployer.deploy(ConceptRegistry)
  let mewAccounts = await getAccounts(deployer.network, web3, true)
  let futureMewMembers = 6
  console.log('---------- DISTRIBUTOR ----------------')
  console.log('... will be able to add ', mewAccounts.length + futureMewMembers, ' members to MEW')
  await deployer.deploy(Distributor, mewAccounts.length + futureMewMembers, ConceptRegistry.address)

  let epochLength = 60 * 60 * 24 * 7
  let waitingPeriod = 60 * 60 * 36
  let tokenReward = 10e9 // 10 Aha
  console.log('---------- MINTER ----------------')
  console.log('tokenReward: ', (tokenReward / 1e9), 'AHA')
  console.log('waitingPeriod: ', (waitingPeriod / 360), ' hours')
  console.log('epochLength: ', epochLength / (360 * 24), 'days')
  await deployer.deploy(Minter, ConceptRegistry.address, epochLength, waitingPeriod, tokenReward)

  let accounts = await getAccounts(deployer.network, web3)
  let initialAmount = 100e9 * (accounts.length + futureMewMembers) // 100 Aha per member
  let initialFundedAccount = accounts[0]
  console.log('---------- FATHOMTOKEN ----------------')
  console.log('initialAmount of Tokens:', initialAmount / 1e9, 'AHA')
  console.log('initial token owner: ', initialFundedAccount)
  await deployer.deploy(
    FathomToken,
    ConceptRegistry.address,
    initialFundedAccount,
    initialAmount,
    Minter.address)
  let minter = await Minter.deployed()
  await minter.init(FathomToken.address)

  let conceptRegistry = await ConceptRegistry.deployed()
  await conceptRegistry.init(FathomToken.address, Distributor.address, ProxyFactory.address)
}
