var ConceptRegistry = artifacts.require('ConceptRegistry')
var FathomToken = artifacts.require('FathomToken')
var Concept = artifacts.require('Concept')
var Minter = artifacts.require('Minter')

var utils = require('../js/utils.js')
var chain = require('../js/assessmentFunctions.js')

let nInitialUsers = 25
let cost = 50
let size = 6
let waitTime = 50
let timeLimit = 1000
let scores = Array(size - 1).fill(100)
scores.push(20)

contract('Minting New Tokens:', function (accounts) {
  let assessment
  let assessment2
  let conceptReg
  let fathomToken
  let minter
  let assessedConcept
  let assessees = accounts.slice(nInitialUsers + 1, nInitialUsers + 3)
  let epochLength

  describe('Initially,', async () => {
    it('a concept is created', async () => {
      conceptReg = await ConceptRegistry.deployed()
      minter = await Minter.deployed()
      let txResult = await conceptReg.makeConcept(([await conceptReg.mewAddress()]), [1000], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress)
      assessedConcept = await Concept.at(txResult.logs[0].args['_concept'])
      fathomToken = await FathomToken.deployed()
      assert.isTrue(await conceptReg.conceptExists.call(assessedConcept.address))
    })

    it('an assessment is run until the end', async () => {
      assessment = await chain.createAndRunAssessment(
        assessedConcept.address,
        assessees[0],
        cost, size, waitTime, timeLimit,
        scores, -1
      )
      let stage = await assessment.instance.assessmentStage.call()
      assert.equal(stage.toNumber(), utils.Stage.Done, 'assessment did not move to stage done')
    })

    describe('During the epoch, the minter...', async () => {
      it('should be initialized', async () => {
        minter = await Minter.deployed()

        await minter.init(fathomToken.address)
        assert.isTrue(await minter.initialized.call(), 'the minter was not initialized during instantiation')
      })

      it('accepts bids from finished assessments', async () => {
        // console.log('assessment', assessment.calledAssessors[0], assessment.address, cost - 3)
        await minter.submitTicket(assessment.calledAssessors[0], assessment.address, cost - 3)
        assert.equal(assessment.calledAssessors[0], (await minter.winner.call()))
      })

      it('updates the winner if the distance of a hashed ticket is smaller', async () => {
        let goal = await minter.epochHash.call()
        // generate 2 times 10 tickets
        let tickets = await utils.generateTickets([assessment], 2, 10)
        for (let ticket of tickets) {
          ticket.hashAsInt = web3.utils.toBN(ticket.hash)
          let ticketDistance = (goal.gt(ticket.hashAsInt)) ? goal.sub(ticket.hashAsInt) : ticket.hashAsInt.sub(goal)

          // submit tickets
          let closestDistanceByMinter = await minter.closestDistance.call()
          await minter.submitTicket(ticket.inputs.assessor, ticket.inputs.assessment, ticket.inputs.tokenSalt)
          let closestDistanceByMinterAfter = await minter.closestDistance.call()

          // and see if closestDistance and winner change as expected
          if (ticketDistance.lt(closestDistanceByMinter)) {
            let winnerByMinter = await minter.winner.call()
            assert.equal(winnerByMinter, ticket.inputs.assessor, 'the assessor was not saved as winner, despite a closer ticket')
            assert.isTrue(closestDistanceByMinterAfter.lt(closestDistanceByMinter), 'the bestDistance of the minter did not decrease despite a closer ticket')
          } else {
            assert.isTrue(closestDistanceByMinterAfter.eq(closestDistanceByMinter), 'the closest distance changed without the ticket being a winner')
          }
        }
      })

      it('rejects bids if the token-salt is too high', async () => {
        assert(await utils.functionReverts(minter.submitTicket.bind(null, assessment.calledAssessors[0], assessment.address, cost + 1), 'salt must be <= stake'))
      })

      it('rejects bids if the assessor is not in the majority cluster', async () => {
        assert(await utils.functionReverts(minter.submitTicket.bind(null, assessment.calledAssessors[size - 1], assessment.address, cost - 1), 'Assessor is not part of majority cluster'))
      })

      it('can not be prompted to mint tokens', async () => {
        assert(await utils.functionReverts(minter.endEpoch.bind(null), 'Epoch and waiting period must be over'), 'function call did not revert')
      })
    })

    describe('After the epoch, there is a waitingPeriod during which the minter...', async () => {
      it('still accepts tickets from the old epoch', async () => {
        epochLength = (await minter.epochLength()).toNumber()
        await utils.evmIncreaseTime(epochLength)
        await minter.submitTicket(assessment.calledAssessors[1], assessment.address, cost - 3)
        // the test for the ticket having gone through is that there is no revert :/
      })

      it('rejects tickets from the new epoch', async () => {
        assessment2 = await chain.createAndRunAssessment(
          assessedConcept.address,
          assessees[1],
          cost, size, waitTime, epochLength, // assessment-params
          scores, -1 // scores & default salts
        )
        let stage = await assessment2.instance.assessmentStage.call()
        assert.equal(stage.toNumber(), utils.Stage.Done, 'assessment did not move to stage done')
        assert(await utils.functionReverts(minter.submitTicket.bind(null, assessment2.calledAssessors[1], assessment2.address, 1), 'Assessment must end before end of epoch'))
      })

      it('but still does NOT allow ending the epoch', async () => {
        assert(await utils.functionReverts(minter.endEpoch.bind(null), 'Epoch and waiting period must be over'),
          'function did not revert (or with wrong reason, see test)')
      })
    })

    describe('After the waiting period, the minter...', async () => {
      it('still allows tickets from the ended epoch', async () => {
        let waitingPeriod = (await minter.waitingPeriod()).toNumber()
        await utils.evmIncreaseTime(waitingPeriod)
        await minter.submitTicket(assessment.calledAssessors[1], assessment.address, cost - 1)
      })

      it('but allows ending the old epoch and mints new tokens to winner', async () => {
        let winner = await minter.winner.call()
        let balanceBefore = await fathomToken.balanceOf.call(winner)
        await minter.endEpoch()
        let balanceAfter = await fathomToken.balanceOf.call(winner)
        assert.isAbove(balanceAfter.toNumber(), balanceBefore.toNumber(), 'winner did not receive minted Tokens')
      })

      it('starts a new epoch', async () => {
        assert(await minter.winner.call(), 0x0, 'winner was not reset')
        assert(await minter.closestDistance.call(), 0, 'winner was not reset')
      })

      it('and starts accepting tickets from the new epoch', async () => {
        await minter.submitTicket(assessment2.calledAssessors[1], assessment2.address, 1)
        let newWinner = await minter.winner.call()
        assert.equal(newWinner, assessment2.calledAssessors[1], 'owner of first ticket did not get saved as winner')
      })

      describe('Coinbase', async () => {
        let coinbaseAddress = accounts[9]
        let altruistUser

        it('users can set a coinbase where they want their winnings to go by default', async () => {
          altruistUser = await minter.winner.call()
          await minter.setCoinbase(coinbaseAddress, {from: altruistUser})
          let userCoinBase = await minter.coinbase.call(altruistUser)
          assert.equal(coinbaseAddress, userCoinBase)
        })

        it('users can redirect their winnings if they want to', async () => {
          epochLength = (await minter.epochLength()).toNumber()
          let balanceBefore = Number(await fathomToken.balanceOf.call(coinbaseAddress))
          let waitingPeriod = (await minter.waitingPeriod()).toNumber()

          await utils.evmIncreaseTime(epochLength + waitingPeriod)
          await minter.endEpoch()

          let balanceAfter = Number(await fathomToken.balanceOf.call(coinbaseAddress))
          let reward = Number(await minter.reward.call())
          assert.equal(balanceAfter, balanceBefore + reward, 'coinbase did not receive all minted tokens')
        })
      })
    })

    describe('Inactive Epochs', async () => {
      let loneUser
      let supplyBefore
      let supplyAfter
      it('A new assessment is run after more than two epochs of inactivity', async () => {
        // finish previous lottery
        await utils.evmIncreaseTime(epochLength * 2)
        await minter.endEpoch()
        // now advance time so that two epochs pass without activity (assessments)
        await utils.evmIncreaseTime(epochLength * 2.5)
        assessment = await chain.createAndRunAssessment(
          assessedConcept.address,
          assessees[1],
          cost, size, waitTime, timeLimit,
          scores, -1
        )
        let stage = await assessment.instance.assessmentStage.call()
        loneUser = assessment.calledAssessors[0]
        assert.equal(stage.toNumber(), utils.Stage.Done, 'assessment did not move to stage done')
      })

      it('a call to endEpoch advances the epoch so that tickets from the new assessment can be submitted', async () => {
        supplyBefore = (await fathomToken.totalSupply.call()).toNumber()
        await minter.endEpoch()
        await minter.submitTicket(loneUser, assessment.address, cost - 3)
        assert.equal(loneUser, await minter.winner.call())
      })

      it('no tokens have been minted for the stale epochs', async () => {
        supplyAfter = (await fathomToken.totalSupply.call()).toNumber()
        assert.equal(supplyAfter, supplyBefore, 'tokens have been minted')
      })
    })
  })
})

contract('Access Control on Minter', function (accounts) {
  let assessment
  let altruistUser
  let conceptReg
  let fathomToken
  let minter
  let assessedConcept
  let assessees = accounts.slice(nInitialUsers + 1, nInitialUsers + 3)

  it('a concept is created', async () => {
    conceptReg = await ConceptRegistry.deployed()
    minter = await Minter.deployed()
    let txResult = await conceptReg.makeConcept(([await conceptReg.mewAddress()]), [1000], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress)
    assessedConcept = await Concept.at(txResult.logs[0].args['_concept'])
    fathomToken = await FathomToken.deployed()
    assert.isTrue(await conceptReg.conceptExists.call(assessedConcept.address))
  })

  it('An assessment is run until the end and a user submits a bid', async () => {
    assessment = await chain.createAndRunAssessment(
      assessedConcept.address,
      assessees[1],
      cost, size, waitTime, timeLimit,
      scores, -1
    )
    let stage = await assessment.instance.assessmentStage.call()
    altruistUser = assessment.calledAssessors[0]
    assert.equal(stage.toNumber(), utils.Stage.Done, 'assessment did not move to stage done')

    await minter.submitTicket(altruistUser, assessment.address, cost - 3)
    assert.equal(altruistUser, await minter.winner.call())
  })

  describe('Staking and Paying for assessments...', async () => {
    it('requires a valid concept', async () => {
      let fakeConcept = utils.zeroAddress
      assert.isFalse(await conceptReg.conceptExists(fakeConcept))
      assert(await utils.functionReverts(fathomToken.takeBalance.bind(null, accounts[0], accounts[1], 50, fakeConcept), 'Concept does not exist'))
    })

    it('can only be happen via valid assessments)', async () => {
      assessment = await chain.createAndRunAssessment(
        assessedConcept.address,
        assessees[1],
        cost, size, waitTime, timeLimit,
        scores, -1
      )
      let stage = await assessment.instance.assessmentStage.call()
      assert.equal(stage.toNumber(), utils.Stage.Done, 'assessment did not move to stage done')
      let validConcept = assessedConcept.address
      let validAssessment = assessment.address
      assert(await utils.functionReverts(fathomToken.takeBalance.bind(null, accounts[1], validAssessment, 50, validConcept, {from: accounts[0]}), 'Assessment access only'))
    })
  })

  describe('Only the Minter-contract', async () => {
    it('can trigger the minting of new tokens', async () => {
      let minter = await fathomToken.minter()
      assert(await utils.functionReverts(fathomToken.mint.bind(null, accounts[1], 12345, {from: accounts[0]}), 'Minter access only'))
      assert.notEqual(minter, accounts[1])
    })
  })

  describe('Only the minter Owner', async () => {
    it('is account[0]', async () => {
      assert.equal(await minter.owner(), accounts[0])
    })

    it('can change the reward', async () => {
      let oldReward = (await minter.reward()).toNumber()
      await minter.setReward(12345)
      let newReward = (await minter.reward()).toNumber()

      assert(await utils.functionReverts(minter.setReward.bind(null, 12345, {from: accounts[1]}), 'Owner access only'))
      assert.notEqual(oldReward, newReward)
      assert.equal(12345, newReward)
    })

    it('can change the epoch length', async () => {
      let oldEpochLength = (await minter.epochLength()).toNumber()
      await minter.setEpochLength(12344)
      let newEpochLength = (await minter.epochLength()).toNumber()

      assert(await utils.functionReverts(minter.setEpochLength.bind(null, 12345, {from: accounts[1]}), 'Owner access only'))
      assert.notEqual(oldEpochLength, newEpochLength)
      assert.equal(12344, newEpochLength)
    })

    it('can change the waiting period iff its less than half the length of the epoch', async () => {
      // only the owner
      assert(await utils.functionReverts(minter.setWaitingPeriod.bind(null, 12, {from: accounts[1]}), 'Owner access only'))

      // only less than half of epochLength
      let epochLength = (await minter.epochLength()).toNumber()
      assert(await utils.functionReverts(minter.setWaitingPeriod.bind(null, epochLength - 1), 'Waiting period must be at most half of epoch'))
      let newWaitingPeriod = epochLength / 2 - 10
      await minter.setWaitingPeriod(newWaitingPeriod)
      assert.equal(newWaitingPeriod, (await minter.waitingPeriod()).toNumber(), 'waiting period was not updated')
    })

    it('can change the minter owner', async () => {
      await minter.setOwner(accounts[1])
      assert.equal(accounts[1], await minter.owner())

      // account[0] can no longer setOwner
      assert(await utils.functionReverts(minter.setOwner.bind(null, accounts[1]), 'Owner access only'))
    })
  })

  describe('Only the FathomToken owner', async () => {
    it('can change the minter', async () => {
      let owner = await fathomToken.owner()
      assert.isTrue(owner === accounts[0])

      assert(await utils.functionReverts(fathomToken.changeMinter.bind(null, utils.zeroAddress, {from: accounts[1]}), 'Owner access only'))

      await fathomToken.changeMinter(accounts[0])
      assert.equal(accounts[0], await fathomToken.minter())
    })

    it('can change the owner', async () => {
      await fathomToken.transferOwnership(accounts[1])
      assert.equal(accounts[1], await fathomToken.owner())

      await fathomToken.changeMinter(accounts[1], {from: accounts[1]})
      assert.equal(accounts[1], await fathomToken.minter())

      assert(await utils.functionReverts(fathomToken.transferOwnership.bind(null, accounts[1]), 'Owner access only'))
    })
  })
})
