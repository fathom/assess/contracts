var ConceptRegistry = artifacts.require('ConceptRegistry')
var ProxyFactory = artifacts.require('ProxyFactory')
var NewAssessment = artifacts.require('NewAssessment')
var Assessment = artifacts.require('Assessment')

var utils = require('../js/utils.js')
var chain = require('../js/assessmentFunctions.js')

contract('Upgrade', (accounts) => {
  let proxyFactory
  let owner
  let timeLimit
  let newAssessment

  it('Only the owner can propose a new Update', async () => {
    proxyFactory = await ProxyFactory.deployed()
    owner = await proxyFactory.owner.call()
    newAssessment = await NewAssessment.deployed()
    timeLimit = (await proxyFactory.timelimit.call()).toNumber()

    // test whether it works for the owner
    await proxyFactory.proposeChange(
      newAssessment.address,
      owner,
      timeLimit,
      {from: owner}
    )
    assert.equal(await proxyFactory.proposedMasterAssessment.call(), newAssessment.address, 'proposed change did not get registered')

    // make sure it only works for the owner
    assert.notEqual(owner, accounts[3])
    assert(await utils.functionReverts(proxyFactory.proposeChange.bind(
      null,
      newAssessment.address,
      owner,
      timeLimit,
      {from: accounts[3]}
    ), 'Owner access only'))
  })

  it('Updates to the assessment contract that do not have bytecode are rejected', async () => {
    assert(await utils.functionReverts(proxyFactory.proposeChange.bind(
      null,
      accounts[5],
      owner,
      timeLimit,
      {from: owner}
    ), 'New assessment-address must be a contract'))
  })

  it('The update can not be implemented before a buffer Period ends', async () => {
    assert(await utils.functionReverts(proxyFactory.implementChange.bind(null, {from: owner}), 'TimeLimit must pass before change can be implemented'))
  })

  it('After the period, the owner can trigger the change to be implemented', async () => {
    await utils.evmIncreaseTime(timeLimit + 10) // wait challenge period
    await proxyFactory.implementChange({from: owner})
    assert(await proxyFactory.masterAssessmentCopy.call(), newAssessment.address, 'proposed change did not get registered')
  })

  it('Newly created assessments use the new distance assessment template (aka no challenge period)', async () => {
    let conceptRegistry = await ConceptRegistry.deployed()
    let assessmentData = await chain.makeAssessment(
      await conceptRegistry.mewAddress.call(),
      accounts[9],
      5,
      5,
      50,
      10000
    )
    let assessmentInstance = await Assessment.at(assessmentData.address)
    let assessors = assessmentData.calledAssessors.slice(0, 5)
    let scores = [100, 100, 100, 100, 100]
    let salts = ['i', 'i', 'i', 'i', 'i']
    let hashes = []
    for (let i = 0; i < assessors.length; i++) {
      hashes.push(utils.hashScoreAndSalt(scores[i], salts[i]))
    }
    await chain.confirmAssessors(assessors, assessmentInstance)
    await utils.evmIncreaseTime(600)
    await chain.commitAssessors(assessors, hashes, assessmentInstance)
    await utils.evmIncreaseTime(10)
    // waiting the challenge period is no longer necessary
    await chain.revealAssessors(assessors, scores, salts, assessmentInstance)
  })
})
