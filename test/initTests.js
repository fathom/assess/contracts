var ConceptRegistry = artifacts.require('ConceptRegistry')
var FathomToken = artifacts.require('FathomToken')
var Concept = artifacts.require('Concept')
let utils = require('../js/utils.js')
var conReg
var aha

contract('ConceptRegistry', function (accounts) {
  var mewAddress
  var createdConceptAddress

  it('should have created the mew contract', async () => {
    conReg = await ConceptRegistry.deployed()
    mewAddress = await conReg.mewAddress.call()

    assert.isTrue(await conReg.conceptExists.call(mewAddress), "mew doesn't exist")
  })

  it('should allow mew as the parent when specified', async () => {
    let txReciept = await conReg.makeConcept([mewAddress], [1000], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress)
    createdConceptAddress = txReciept.logs[0].args['_concept']

    let createdConcept = await Concept.at(createdConceptAddress)

    assert.equal(await createdConcept.parents.call(0), mewAddress)
  })

  it('should set the parents if specified', async () => {
    let txReceipt = await conReg.makeConcept([createdConceptAddress, mewAddress], [100, 900], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress)
    let childConcept = await Concept.at(txReceipt.logs[0].args['_concept'])
    assert.equal(await childConcept.parents.call(0), createdConceptAddress, "Child concept doesn't know supplied parent")
  })

  it('should throw if no parents are specified', async () => {
    assert(await utils.functionReverts(conReg.makeConcept.bind(null, [], [], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress), 'Must specify at least one parent concept'))
  })

  it('should throw if too many parents are specified', async () => {
    assert(await utils.functionReverts(
      conReg.makeConcept.bind(
        null,
        [accounts[0], accounts[1], accounts[2], accounts[3], accounts[4], accounts[5]],
        [100, 100, 100, 100, 100, 500],
        60 * 60 * 24,
        '0x00',
        utils.zeroAddress,
        utils.zeroAddress
      ), 'Too many parents'
    ))
  })

  it('should throw if parents do not exists invalid', async () => {
    assert(await utils.functionReverts(conReg.makeConcept.bind(null, [accounts[0]], [1000], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress), 'Parent concept does not exist'))
  })

  it('should throw if the sum of parent factors is != 1000', async () => {
    assert(await utils.functionReverts(conReg.makeConcept.bind(null, [createdConceptAddress, mewAddress], [400, 400], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress), 'Parent factors do not add up to 1000'))
  })

  it('should throw if parent factor is > 1000', async () => {
    assert(await utils.functionReverts(conReg.makeConcept.bind(null, [createdConceptAddress], [1100], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress), 'Parent factor must be <1000'))
  })
})

contract('Initial User', function (accounts) {
  it('should have a balance of 700 AHAs', async () => {
    aha = await FathomToken.deployed()
    let balance = await aha.balanceOf.call(accounts[0])
    assert.equal(balance.toNumber(), 100e9 * 7, 'User0 does not have 700 AHA')
    balance = await aha.balanceOf.call(accounts[4])
    assert.equal(balance.toNumber(), 100e9, 'User4 does not have 100 AHA')
  })
})

contract('token transfers', function (accounts) {
  it('Should modify balances correctly', async () => {
    var account1InitialBalance = await aha.balanceOf.call(accounts[0])
    var account2InitialBalance = await aha.balanceOf.call(accounts[1])
    var amount = 50

    await aha.transfer(accounts[1], amount, {from: accounts[0]})

    assert.equal(account1InitialBalance - await aha.balanceOf.call(accounts[0]), amount)
    assert.equal(await aha.balanceOf.call(accounts[1]) - account2InitialBalance, amount)
  })
})
