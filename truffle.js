var HDWalletProvider = require('truffle-hdwallet-provider')
let mnemonic
if (process.env.DEPLOY_SEED) {
  mnemonic = process.env.DEPLOY_SEED
  console.log('using environment seed')
} else {
  try {
    mnemonic = require('./secrets.json')
  } catch (e) {
    console.log('no mnemonic found. Deploying to main- or test-nets will not work.')
  }
}

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // Match any network id
      rpc: {
        host: 'localhost',
        port: 8545
      }
    },
    rinkeby: {
      provider: () => new HDWalletProvider(mnemonic, 'https://rinkeby.infura.io/v3/b5afe7c5e5a34359a1852ad30d50fa48'),
      network_id: 4
    },
    kovan: {
      provider: () => new HDWalletProvider(mnemonic, 'https://kovan.infura.io/v3/b5afe7c5e5a34359a1852ad30d50fa48'),
      network_id: 42,
      skipDryRun: true
    }
  },
  compilers: {
    solc: {
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        }
      }
    }
  }
}
