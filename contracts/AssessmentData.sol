pragma solidity ^0.5.0;

// NOTE:
// The architecture of Assessment- AssessmentData- and AssessmentProxy-contract
// has been modelled upon
// https://blog.gnosis.pm/solidity-delegateproxy-contracts-e09957d0f201 by Alan Lu

import "./Math.sol";
import "./Concept.sol";
import "./FathomToken.sol";
import "./Proxy.sol";


/**
   @dev Contract holding all parameters for assessor calling, assessment
   completion and score calculation that might to be tuned in future versions of
   the network.
*/
contract AssessmentParameters {
    // For each concept, at most NofMembers * memberCeilingFactor/10 are going to be called
    uint public constant MEMBERCALL_CEILING_FACTOR = 5;

    // Minimal size for an assessment to be valid
    uint public constant MIN_ASSESSMENT_SIZE = 5;

    // An assessment of size x will try sampling x * minAssessmentPoolFactor/10 assessors
    uint public constant ASSESSORPOOL_SIZEFACTOR = 20;

    // Mandatory time period between last commit and first reveal to enable steals
    uint public constant CHALLENGE_PERIOD = 12 hours;

    // Max distance two scores can be apart to be seen as agreeing
    uint public constant CONSENT_RADIUS = 13; // 5% of the total range of 256
}


// @dev Header contract to dedepulicate event-spefication.
contract AssessmentHeader {
    event DataChanged(address user, bytes oldData, bytes newData);
}


/// @dev Contract with all state-declarations of an assessment.
// solhint-disable-next-line max-states-count
contract AssessmentData is ProxyData, AssessmentHeader, AssessmentParameters {
    // ********************************************
    // PUBLIC
    // ********************************************
    Concept public concept;

    FathomToken public fathomToken;

    address public assessee;

    // Point in unix-time to mark...
    //     1) the latest possible time to confirm (aka // assessment startTime) and
    //     2) earliest time to reveal
    uint public checkpoint;

    // Point in unix-time to mark...
    //  1) the latest time by which assessors can still reveal and
    //  2) the actual time the last assessor has revealed
    uint public endTime;

    // Number of assessors
    uint public size;

    // Tokens paid per assessor
    uint public cost;

    // >0 pass
    // <0 fail
    // 0 incomplete, no consensus
    int public finalScore;

    // Counter how many assessors have committed/revealed their score
    uint public done;

    // Salt that will be used for ticket-generation in the token-minting-lottery
    bytes32 public salt;

    // Participant (assessor or assessee) => data (e.g. IPFS hashes)
    mapping (address => bytes) public data;

    // Assessor- and assessment-status
    enum Stage {
        None,
        Called,
        Confirmed,
        Committed,
        Done,
        Burned,
        Dissent
    }

    // read: if an assessor is in stage x, he was/has successfully x'ed.
    mapping (address => Stage) public assessorStage;

    // read: If an assessment is in stage x, ALL assessors have successfully x'ed
    Stage public assessmentStage;

    // ********************************************
    // INTERNAL
    // ********************************************
    // assessors that have staked
    address[] internal assessors;

    // assessor => sha32(score, salt)
    mapping(address => bytes32) internal commits;

    // assessor => score
    mapping(address => int128) internal scores;
}


/**
   @dev Contract with state declarations as AssessmentData but with all
   visibility modifiers set to internal. That way the AssessmentProxy can
   inherit from it (to have an identical storage layout) but will not have all
   the public getters.
*/
// solhint-disable-next-line max-states-count
contract AssessmentDataInternal is ProxyData, AssessmentHeader, AssessmentParameters {
    Concept internal concept;
    FathomToken internal fathomToken;
    address internal assessee;

    uint internal checkpoint;
    uint internal endTime;
    uint internal size;
    uint internal cost;
    int internal finalScore;
    uint internal done;
    bytes32 internal salt;

    mapping (address => bytes) internal data;

    enum Stage {
        None,
        Called,
        Confirmed,
        Committed,
        Done,
        Burned,
        Dissent
    }

    mapping (address => Stage) internal assessorStage;
    Stage internal assessmentStage;

    address[] internal assessors;
    mapping(address => bytes32) internal commits;
    mapping(address => int128) internal scores;
}
