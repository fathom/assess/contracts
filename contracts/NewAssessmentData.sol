pragma solidity ^0.5.0;

import "./Math.sol";
import "./Concept.sol";
import "./FathomToken.sol";
import "./Proxy.sol";


contract NewAssessmentParameters {
    //for each concept, at most NofMembers * memberCeilingFactor/10 are going to be called
    // (5 -> half the members, 2 -> 20% of the members, ...)
    uint constant MEMBERCALL_CEILING_FACTOR = 5;
    // minimal size for an assessment to be valid
    uint constant MIN_ASSESSMENT_SIZE = 5;
    // an assessment of size x will have to try sampling x * minAssessmentPoolFactor/10 assessors
    // (so use 20 as 2, 15 as 1.5 ...)
    uint constant ASSESSORPOOL_SIZEFACTOR = 20;
    uint constant CHALLENGE_PERIOD = 0;
    // Max distance two scores can be apart to be seen as agreeing
    uint constant public CONSENT_RADIUS = 13; // 13 is 5% of the total range of 256
}


contract NewAssessmentHeader {
    event DataChanged(address user, bytes oldData, bytes newData);
}


contract NewAssessmentData is ProxyData, NewAssessmentHeader, NewAssessmentParameters {
    Concept public concept;

    FathomToken public fathomToken;

    address public assessee;

    uint public checkpoint;

    uint public endTime;

    uint public size;

    uint public cost;

    int public finalScore;

    uint public done;

    bytes32 public salt;

    mapping (address => bytes) public data;

    enum Stage {
        None,
        Called,
        Confirmed,
        Committed,
        Done,
        Burned,
        Dissent
    }

    mapping (address => Stage) public assessorStage;

    Stage public assessmentStage;

    address[] internal assessors;

    mapping(address => bytes32) internal commits;

    mapping(address => int128) internal scores;
}
