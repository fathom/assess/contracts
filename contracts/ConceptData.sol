pragma solidity ^0.5.0;

// NOTE:
// The architecture of Concept- ConceptData- and ConceptProxy-contract
// has been modelled upon
// https://blog.gnosis.pm/solidity-delegateproxy-contracts-e09957d0f201 by Alan Lu

import "./FathomToken.sol";
import "./ConceptRegistry.sol";
import "./Assessment.sol";
import "./Math.sol";
import "./Proxy.sol";


/**
   @dev Header contract to dedepulicate event-spefication.
*/
contract ConceptHeader {
    event DataSet(address _owner, bytes _data);
}


/**
   @dev Contract with all state-declarations of an assessment.
*/
contract ConceptData is ProxyData, ConceptHeader {
    // ********************************************
    // PUBLIC
    // ********************************************
    address public owner;

    FathomToken public fathomToken;

    ConceptRegistry public conceptRegistry;

    // Point in unix time when this contract was created
    uint public creationDate;

    // Address of the original concept, if this concept is a clone
    address public cloneOf;

    // The concepts that this concept is child to
    address[] public parents;

    // List of degrees of similarity between this concept and its parents (adds up to
    // 1000)
    uint[] public parentFactors;

    // Data stored by owner, possible source of truth about what the concept is about
    bytes public data;

    // Period of unix-time: How long are assessments in this concept valid
    // before they need to be retaken
    uint public lifetime;

    // assessmentAddress => createdByThisConcept-flag, to avoid illegitimate assessments
    mapping (address => bool) public assessmentExists;

    struct MemberData {
        // Address of entry assessment (empty for initial members!)
        address recentAssessment;

        // bucketNumber => Position in List + 1 (0 means not in the list)
        mapping(uint => uint) bucketIndices;

        // Variable used for probabilistical drawing (= score * size of majority cluster)
        uint weight;

        // Date when membership began
        uint assessmentDate;
    }

    // member => info
    mapping (address => MemberData) public memberData;

    // 'buckets' is a mapping used track when members are available to act as
    // assessor. Specifically, time is discretized into intervals (or buckets)
    // of length lifetime/NofBUCKETS. When members complete an assessment they
    // are added to the current active bucket and the next NofBUCKETS-1.
    // As each point in time corresponds to one bucket (the bucketNumber), it
    // is possible to know in advance whether there are enough potential assessor to
    // create a valid assessment.
    // bucketNumber => list of available members
    mapping (uint => address[]) public buckets;

    // ********************************************
    // INTERNAL
    // ********************************************
    // Constant parameter determining how fine or course time is
    // discretized into buckets. Example: If an assessment has lifetime of 10 months and
    // NofBUCKETS=5, new members will be available as assessors for at least 8
    // months but less than 10.
    uint internal constant N_OF_BUCKETS = 5;
}

/**
   @dev Contract with state declarations as ConceptData but with all
   visibility modifiers set to internal. That way the ConceptProxy can
   inherit from it (to have an identical storage layout) but will not have all
   the public getters.
*/

contract ConceptDataInternal is ProxyData, ConceptHeader {
    address internal owner;
    FathomToken internal fathomToken;
    ConceptRegistry internal conceptRegistry;
    uint internal creationDate;
    address internal cloneOf;
    address[] internal parents; //The concepts that this concept is child to (ie: Calculus is child to Math)
    uint[] internal parentFactors;
    bytes internal data;
    uint internal lifetime;

    mapping (address => bool) internal assessmentExists;

    struct MemberData {
        address recentAssessment;
        mapping(uint => uint) bucketIndices;
        uint weight;
        uint assessmentDate;
    }

    mapping (address => MemberData) internal memberData;

    mapping (uint => address[]) internal buckets;

    uint internal constant N_OF_BUCKETS = 5;
}
