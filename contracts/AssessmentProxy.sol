pragma solidity ^0.5.0;

// NOTE:
// The architecture of Assessment- AssessmentData- and
// AssessmentProxy-contract has been modelled upon
// https://blog.gnosis.pm/solidity-delegateproxy-contracts-e09957d0f201 by Alan Lu

import "./Proxy.sol";
import "./AssessmentData.sol";


/**
   @dev ProxyContract for Assessment. For the assessment logic see
   ./Assessment.sol and for state declarations see ./AssessmentData.sol
 */
contract AssessmentProxy is Proxy, AssessmentDataInternal {
    constructor(address _proxied,
                address _concept,
                address _assessee,
                uint _cost,
                uint _size,
                uint _confirmTime,
                uint _timeLimit) public Proxy(_proxied) {
        assessee = _assessee;
        concept = Concept(_concept);
        fathomToken = concept.fathomToken();

        endTime = now + _timeLimit;
        // set checkpoint to latest possible time to confirm
        checkpoint = now + _confirmTime;
        assert(checkpoint < endTime);

        size = _size;
        cost = _cost;

        fathomToken.emitNotification(assessee, FathomToken.Note.StartedAnAssessment);
        done = 0;
    }
}

