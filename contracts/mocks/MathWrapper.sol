pragma solidity ^0.5.0;

import { Math } from "../Math.sol";


/*
  this is a wrapper contract needed to avoid a bug in the solidity-coverage tool that prevents
  running tests on pure functions. See this link for more context:
  https://github.com/sc-forks/solidity-coverage/issues/234
*/
contract MathWrapper {

    function getFinalScore(int[] memory data, uint consentRadius) public pure returns(int finalScore, uint largestClusterSize) {
        return Math.getFinalScore(data, consentRadius);
    }

    function getPayout(uint distance, uint stake, uint consentRadius) public pure returns(uint payout, bool dissenting) {
        return Math.getPayout(distance, stake, consentRadius);
    }
}
