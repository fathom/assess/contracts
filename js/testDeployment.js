let conceptRegArtifact = require('../build/contracts/ConceptRegistry.json')
let conceptArtifact = require('../build/contracts/Concept.json')
let fathomTokenArtifact = require('../build/contracts/FathomToken.json')
let distributorArtifact = require('../build/contracts/Distributor.json')

let provider
const network = process.argv[2]

async function create () {
  const Web3 = require('web3')

  // detect network and declare variables accordingly
  if (network === 'rinkeby') {
    let truffleConfig = await require('../truffle.js')
    provider = await truffleConfig.networks.rinkeby.provider()
  } else if (network === 'kovan') {
    let truffleConfig = await require('../truffle.js')
    provider = await truffleConfig.networks.kovan.provider()
  } else {
    // use Ganache by default (last deployed contract version)
    provider = 'http://localhost:8545'
  }
  const web3 = new Web3(provider)
  const eth = web3.eth

  console.log('\n### -- 1. Set Up -- ###\n')

  // log accounts
  let accounts = await eth.getAccounts()
  if (network === 'kovan' || network === 'rinkeby') {
    // if not on testnet
    console.log('fetching initial member list...')
    accounts = require('../initialMembers.json')
  }
  console.log('++++++++++= accounts : ')
  console.log(accounts)
  // log network
  let net = await eth.net.getId()
  console.log('current network version')
  console.log(net)

  console.log('\n### -- 2. Instanciate Contracts -- ###\n')

  // get the mewConcept
  let conceptRegAddress = conceptRegArtifact.networks[net].address
  let conceptReg = await new web3.eth.Contract(conceptRegArtifact.abi, conceptRegAddress, {from: accounts[0]})
  let mewAddress = await conceptReg.methods.mewAddress().call()
  let mewConcept = await new web3.eth.Contract(conceptArtifact.abi, mewAddress, {from: accounts[0]})

  // fathomToken
  let fathomTokenAddress = fathomTokenArtifact.networks[net].address
  let fathomToken = await new web3.eth.Contract(fathomTokenArtifact.abi, fathomTokenAddress, {from: accounts[0]})

  // distributor
  let distributorAddress = distributorArtifact.networks[net].address
  let distributor = await new web3.eth.Contract(distributorArtifact.abi, distributorAddress, {from: accounts[0]})
  console.log('done...')

  console.log('\n### -- 2. Check for initial members and funding -- ###\n')

  // see if all members have been added to mew
  // see capacity of distributor
  let nInitialMembers = Number(await distributor.methods.nInitialMembers().call())
  console.log('capacity of distributor', nInitialMembers)

  // see who has been added to mew by the distributor
  let mewMemberAddresses = []
  for (var i = 0; i < nInitialMembers; i++) {
    mewMemberAddresses.push(await distributor.methods.memberAddresses(i).call())
  }
  console.log('those are the addresses in mew', mewMemberAddresses)

  let added = 0
  for (let i = 0; i < accounts.length; i++) {
    if (Number(await mewConcept.methods.getWeight(accounts[i]).call()) > 0) {
      console.log('initial Member', accounts[i], ' was added to mew')
      added++
    } else {
      console.log('initial Member', accounts[i], ' was NOT added to mew')
    }
  }
  // await distributor.methods.addInitialMember(accounts[i])

  // see if everyone is funded
  let funded = 0
  for (let i = 0; i < accounts.length; i++) {
    let balance = Number(await fathomToken.methods.balanceOf(accounts[i]).call())
    if (balance === 0) {
      console.log('initial Member', accounts[i], ' was not funded')
      // await distributor.methods.addInitialMember(accounts[i])
    } else {
      console.log('initial Member', accounts[i], ' was funded with ', balance)
      funded++
    }
  }

  console.log('\n### -- 3. Results -- ###\n')
  if (added === nInitialMembers) {
    console.log(' added to mew: OK')
  } else {
    console.log('not all were added')
  }
  console.log(added, ' out of ', nInitialMembers)
  if (funded >= nInitialMembers) {
    console.log(' funded: OK')
  } else {
    console.log('not all were funded')
  }
  console.log(funded, ' out of ', nInitialMembers)
}

async function execute () {
  await create()
  process.exit()
}

execute()
